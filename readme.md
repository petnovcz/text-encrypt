## Simple text encrypt/decrypt tool

Simple command line tool for text encryption of user provided text and password.

## Install
- a) Use binaries from /bin directory
- b) Build manually using go build
```commandline
go build main.go
```

On MacOS Big Sur and later to run tool from command line, use wget - e.g.:
```commandline
wget https://gitlab.com/petnovcz/text-encrypt/-/raw/master/bin/text-encrypt-amd64-darwin
chmod a+x text-encrypt-amd64-darwin
text-encrypt-amd64-darwin encrypt
```

## Usage
Run the tool with following arguments
```commandline
text-encrypt encrypt
text-encrypt decrypt
```

