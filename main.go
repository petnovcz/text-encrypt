package main

import (
	"bufio"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"io"
	"os"
)

const CommandEncrypt string = "encrypt"
const CommandDecrypt string = "decrypt"

func main() {
	if len(os.Args) != 2 {
		fmt.Println("USAGE: cipher-text {encrypt|decrypt}")
		return
	}

	// encrypt or decrypt?
	command := os.Args[1]

	if command == CommandEncrypt {
		encryptCommand()
	} else if command == CommandDecrypt {
		decryptCommand()
	}
}

// encrypt text
func encryptCommand() {
	// prepare input variables
	var userText string
	var password string

	// take user input
	fmt.Print("Enter text for encrypt: ")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	userText = scanner.Text()

	fmt.Print("Enter your password: ")
	fmt.Scanln(&password)

	// create secret from password
	secret := SecretFromPassword(password)

	// encrypt user text
	encryptedText, err := EncryptText(userText, secret)
	if err != nil {
		fmt.Println("error while encrypting text: ", err)
	}
	fmt.Println("Encrypted text:")
	fmt.Println(encryptedText)
}

// decrypt text
func decryptCommand() {
	// prepare input variables
	var userText string
	var password string

	// take user input
	fmt.Print("Enter text to decrypt: ")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	userText = scanner.Text()

	fmt.Print("Enter your password: ")
	fmt.Scanln(&password)

	// create secret from password
	secret := SecretFromPassword(password)

	// encrypt user text
	decryptedText, err := DecryptText(userText, secret)
	if err != nil {
		fmt.Println("error while decrypting text: ", err)
	}
	fmt.Println("Decrypted text:")
	fmt.Println(decryptedText)
}

// SecretFromPassword prepare secret from password
// secret has to be 16, 24 or 32 bytes, so we use hash (SHA1) from password and take first 24 bytes
func SecretFromPassword(password string) string {
	h := sha1.New()
	h.Write([]byte(password))
	secret := h.Sum(nil)
	secretBytes := secret[0:24]

	return string(secretBytes)
}

// EncryptText Encrypt text and return its base64 encrypted representation
func EncryptText(text, secret string) (string, error) {
	block, err := aes.NewCipher([]byte(secret))
	if err != nil {
		return "", err
	}

	cipherText := make([]byte, aes.BlockSize+len(text))
	iv := cipherText[:aes.BlockSize]
	if _, err = io.ReadFull(rand.Reader, iv); err != nil {
		return "", fmt.Errorf("could not encrypt: %v", err)
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(cipherText[aes.BlockSize:], []byte(text))

	// return base64 encoded text
	return base64.StdEncoding.EncodeToString(cipherText), nil
}

// DecryptText Decrypt encrypted text
func DecryptText(text, secret string) (string, error) {
	cipherText, err := base64.StdEncoding.DecodeString(text)
	if err != nil {
		return "", fmt.Errorf("invalid encrypted text: %v", err)
	}

	block, err := aes.NewCipher([]byte(secret))
	if err != nil {
		return "", fmt.Errorf("invalid secret: %v", err)
	}

	if len(cipherText) < aes.BlockSize {
		return "", fmt.Errorf("invalid encrypted text")
	}

	iv := cipherText[:aes.BlockSize]
	cipherText = cipherText[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)
	stream.XORKeyStream(cipherText, cipherText)

	return string(cipherText), nil
}
